# -*- mode: ruby -*-
# vi: set ft=ruby :

$ORIGIN    = "git@github.uc.edu:Bioreactor"
$REPO_NAME = "bioreactor-vm"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "bioreactor-jessie"
  config.vm.define :debian_jessie

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine.
  config.vm.network "forwarded_port", guest: 80, host: 9980, id: "http"
  # See https://github.com/mitchellh/vagrant/issues/3232#issuecomment-48994417
  config.vm.network "forwarded_port", guest: 22, host: 9922, id: "ssh"
  config.vm.network "forwarded_port", guest: 5000, host: 55000, id: "flask-app"
  config.vm.network "forwarded_port", guest: 5001, host: 55001, id: "flask-api"

  # Forward SSH authentication credentials to the VM; disabled for now because
  # it gets confusing when things like 'git clone' work inside the VM when you
  # didn't expect them to.
  # See http://docs.vagrantup.com/v2/vagrantfile/ssh_settings.html
  #config.ssh.forward_agent = true

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "bioreactor", "/bioreactor", group: "www-data", \
    mount_options: ["fmode=775", "dmode=775"]

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
    # Uncomment to run with a GUI (so you can see what's going on):
    # See also: https://docs.vagrantup.com/v2/virtualbox/configuration.html
    #vb.gui = true
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "80"]
    # Might want these if you were, say, running a memory hog Java app, or X
    # with a graphical desktop environment
    #vb.memory = "1024"
    #vb.customize ["modifyvm", :id, "--vram", "32"]
  end

  # An example of how to copy batches of files to the VM before provisioning
  #files = {
  #  'provisioning/files/origin_host_key' => '~/.ssh/known_hosts',
  #}
  #files.each { |s,d| config.vm.provision "file", source: s, destination: d }

  # Provision from within the VM itself by running the 'self-provision.sh'
  # script from within the (default) '/vagrant' shared folder.
  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    echo "==> Running provisioning playbooks with Ansible <=="
    /vagrant/bin/self-provision.sh
  SHELL

end
# vim: sw=2 ts=2 expandtab
