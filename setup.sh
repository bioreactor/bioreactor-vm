#!/bin/bash
#
#  Just download the base box and do the 'vagrant up' for the user already.
#
#  Author:  Kevin Ernst <ernstki@mail.uc.edu>
#  Date:    27 Sep 2016
#
#set -x
ME=$( basename "$BASH_SOURCE" )
MYDIR=$( cd "$(dirname "$BASH_SOURCE")" && pwd )
# FIXME: nothing actually at this URL yet
VAGRANT_BOX=${VAGRANT_BOX:-https://tf.cchmc.org/external/ern6xv/bioreactor-jessie.box}
BOX_NAME=bioreactor-jessie

quietly() {
    $* &>/dev/null
}
carp() {
    echo "$*" >&2
}
croak() {
    carp
    carp "ACK! $*"
    carp "     Bailing out..."
    carp
    exit 1
}

# Change into the directory where this script lives, in case it was run from
# a relative pathname (or a pathname starting from '/'):
trap 'quietly popd' 0 1 2 15
quietly pushd "$MYDIR"

clear
echo "VM IMAGE PREPARATION"
echo "--------------------"
echo
echo "  This script will download and install the '$BOX_NAME' Vagrant"
echo "  base box and run 'vagrant up' for you."
echo 
echo "  If you didn't already, you'll want to download and install VirtualBox"
echo "  and Vagrant for your OS before continuing."
echo
echo "    https://www.virtualbox.org/wiki/Downloads"
echo "    https://www.vagrantup.com/downloads.html"
echo
read -p "Press ENTER now to continue or CTRL+C to abort... "
echo

if ! quietly which vagrant; then
    croak "Unable to find 'vagrant' in your \$PATH"
fi

if [ ! -f bioreactor/requirements.txt ]; then
    if ! quietly which git; then
        carp "
Without Git installed, I can't clone the 'bioreactor' submodule, and the
Vagrant provisioning process WILL fail.

You can still allow this script to continue to download the base box if you
want, but please go install Git (e.g., 'sudo apt-get install git').
"
        read -p "Press ENTER now to continue or CTRL+C to abort... "
        echo
        #croak "Unable to find 'git' in your \$PATH. Giving up."
    else
        echo "
Cloning the 'bioreactor' repository so we can serve it from inside the VM.

You may be prompted for your Git provider (e.g. GitHub) credentials here.
"
        git submodule init
        git submodule update
        echo
    fi
fi

# Now download the base box and unpack it (to ~/.vagrant.d/boxes, probably)
if vagrant box list 2>&1 | grep -q "^$BOX_NAME"; then
    carp "Vagrant box '$BOX_NAME' seems to be present already. Skipping download."
else
    vagrant box add $BOX_NAME $VAGRANT_BOX || \
        croak "There was a problem downloading / unpacking the Vagrant box"
fi

vagrant up || \
    croak "There was a problem running / provisioning the Vagrant box"

echo
echo "Looks like everything was successful, at least as far as this part of"
echo "the setup goes. If Vagrant bailed out during the provisioning step,"
echo "you can safely run 'vagrant provision' here to retry."
echo
echo "Otherwise, you may now use SSH to connect to the running VM on port"
echo "9922 (simply type 'vagrant ssh' if you're on Linux or Mac OS X) or try"
echo "http://localhost:9980 to access the VM's web server."
echo

# vim: ts=4 sw=4 tw=78 colorcolumn=78
