# Installing Slurm on the VM

_NB: This is not a hard requirement for Bioreactor development, unless
you're working on the cluster (job scheduler) interface._

[Slurm][] is an open source workload manager (sometimes called a
"[job scheduler][jobschedwiki]," "batch scheduler," "job manager," or
something along those lines) which is similar to the Platform LSF scheduler
used in the Children's research division.

The process is not yet automatic (_i.e._, it is not performed as part of the
initial `vagrant up` provisioning), but there exists an Ansible playbook
in this repository with which you can easily install the necessary services.
This gives you a "mini-cluster" on the VM to which you can send batch jobs.

After the initial provisioning process is complete, SSH into the VM and run
the playbook like this:

```bash
# these steps performed while ssh'd into the VM as the 'vagrant' user
cd ~/devel/bioreactor-vm
bin/play.sh provisioning/set_up_slurm.yml
```

and hopefully, on a good day, that's it!

**SECURITY NOTE**: This will set up a MySQL server and a `slurm` database user
where both passwords can easily be read from the Ansible `group_vars/all`
config file in this repository. If deploying to a "production" server with
this same playbook, you'll need to choose real passwords. The process for
MySQL 5.5 is documented [here][mysqlpass].

### Testing to make sure it worked

You can test that the queue manager works like this (as a non-privileged
user):

```bash
cd # make sure you're in a writable directory like $HOME
sbatch <<<'#!/bin/bash
#SBATCH -J "First test job"
ls -lR'

# then, to make sure it worked
sacct -c
```

and the results should look something like this:
```
       JobID    UID    JobName  Partition   NNodes        NodeList      State                 End
------------ ------ ---------- ---------- -------- --------------- ---------- -------------------
2              1000 First tes+      debug        1        bioreactor  COMPLETED 2016-07-13T21:57:53
```

### See also

* "[Installing Slurm for testing][slurmwiki]" article on the
  [Bioreactor wiki][bioreactorwiki] - _for the manual steps necessary to accomplish
  what the playbook already does for you_

[slurm]: http://slurm.schedmd.com/slurm.html
[slurmacct]: http://slurm.schedmd.com/accounting.html
[jobschedwiki]: https://en.wikipedia.org/wiki/Job_scheduler
[slurmwiki]: https://FIXME/Bioreactor/bioreactor/wiki/Installing-Slurm-for-testing
[bioreactorwiki]: https://github.uc.edu/Bioreactor/bioreactor/wiki
[mysqlpass]: https://dev.mysql.com/doc/refman/5.5/en/set-password.html
